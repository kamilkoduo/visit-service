package com.kamil.visitservice;

import java.sql.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Database {

    private Connection connection = null;

    public Database() {

        String url = "jdbc:postgresql://postgres/postgres?user=postgres&password=1&ssl=false";
        // String user = "postgres";
        // String password = "1";
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url);
        } catch (Exception e) {
            System.out.println("DB error: " + e);
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void postVisit(Visit visit) {
        String sql = "INSERT INTO visits VALUES (?,?,?)";
        try {
            PreparedStatement st = getConnection().prepareStatement(sql);
            st.setTimestamp(1, visit.getTimestamp());
            st.setInt(2, visit.getUserId());
            st.setInt(3, visit.getPageId());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }
    }
    private int executeSQL(String sql){
        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }
        return -1;
    }

    //get all visits number
    public int getAllVisitsNumber(Timestamp ts1, Timestamp ts2){
        String sql = "SELECT COUNT(*) FROM visits WHERE ts>='"+ts1+"' AND ts<='"+ts2+"'";

        return executeSQL(sql);
    }
    //get unique users number
    public int getUniqueUsersNumber(Timestamp ts1, Timestamp ts2){
        String sql ="SELECT COUNT(DISTINCT user_id) FROM visits WHERE ts>='"+ts1+"' AND ts<='"+ts2+"'";

        return executeSQL(sql);
    }
    //get regular users number
    private int getRegularUsersNumber(Timestamp ts1, Timestamp ts2){
        String sql ="SELECT COUNT(*) FROM " +
                "(SELECT user_id, COUNT(user_id) AS count FROM visits WHERE ts>='"+ts1+"' AND ts<='"+ts2+"' GROUP BY user_id) AS A " +
                "WHERE A.count>=10";

        return executeSQL(sql);
    }

    public Map<String, Integer> serveQuery1(Timestamp ts1, Timestamp ts2){
        Map <String, Integer> response = new HashMap<>();
        response.put("Количество посещений за сутки", getAllVisitsNumber(ts1,ts2));
        response.put("Количество уникальных пользователей за сутки", getUniqueUsersNumber(ts1,ts2));
        return response;
    }

    public Map<String, Integer> serveQuery2(Timestamp ts1, Timestamp ts2){
        Map <String, Integer> response = new HashMap<>();
        response.put("Количество посещений за указанный период", getAllVisitsNumber(ts1,ts2));
        response.put("Количество уникальных пользователей за указанный период", getUniqueUsersNumber(ts1,ts2));
        response.put("Количество постоянных пользователей за указанный период",getRegularUsersNumber(ts1,ts2));
        return response;
    }


}
