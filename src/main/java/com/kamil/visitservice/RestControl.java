package com.kamil.visitservice;

import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Map;

@RestController
@RequestMapping("service")
public class RestControl {

    private Database db = new Database();

    private Map<String, Integer> servePost(Integer userId, Integer pageId, Timestamp ts) {
        Visit visit = new Visit(userId, pageId, ts);
        db.postVisit(visit);

        Timestamp start = new Timestamp(ts.getTime() - ts.getTime() % (1000 * 60 * 60 * 24));
        Timestamp end = new Timestamp(ts.getTime() - ts.getTime() % (1000 * 60 * 60 * 24) + (1000 * 60 * 60 * 24 - 1));

        return db.serveQuery1(start, end);
    }


    private Map<String, Integer> serveGet(Long start, Long end) {
        Timestamp tStart = new Timestamp(start);
        Timestamp tEnd = new Timestamp(end);

        return db.serveQuery2(tStart,tEnd);
    }

    @PostMapping(value = "/visit", params = {"user_id", "page_id"})
    public Map<String, Integer> post(@RequestParam("user_id") Integer userId, @RequestParam("page_id") Integer pageId) {
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        return servePost(userId, pageId, ts);
    }

    @PostMapping(value = "/visit", params = {"user_id", "page_id", "time"})
    public Map<String, Integer> post(@RequestParam("user_id") Integer userId, @RequestParam("page_id") Integer pageId, @RequestParam("time") Long time) {
        Timestamp ts = new Timestamp(time);
        return servePost(userId, pageId, ts);
    }

    @GetMapping(value = "stats", params = {"start", "end"})
    public Map<String, Integer> get(@RequestParam("start") Long start, @RequestParam("end") Long end) {
        return serveGet(start, end);
    }

}
