package com.kamil.visitservice;

import java.sql.Timestamp;

public class Visit {
    private Integer userId;
    private Integer pageId;
    private Timestamp timestamp;

    public Visit(int userId, int pageId, Timestamp timestamp) {
        this.userId = userId;
        this.pageId = pageId;
        this.timestamp = timestamp;
    }
    public Visit(){}


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }


    public Integer getPageId() {
        return pageId;
    }

    public void setPageId(Integer pageId) {
        this.pageId = pageId;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }


}
