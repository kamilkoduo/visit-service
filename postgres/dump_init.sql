CREATE TABLE visits(
  ts TIMESTAMP NOT NULL,
  user_id INT NOT NULL,
  page_id INT NOT NULL,
  
  PRIMARY KEY (ts, user_id, page_id)
);
