Visit service 

Instructions:
1. run `./gradlew build`
2. run `./gradlew dockerCreateDockerfile`
3. run `docker-compose up --build`

Now you can send requests like:  
POST: `http://localhost:9090/service/visit?user_id=10&page_id=5`   

GET: `http://localhost:9090/service/stats?start=0&end=1000000000000000`